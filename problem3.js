// const carsInventory = require("./cars.js");

function carListingAlphabetically(carsInventory) {
    
    if(carsInventory === undefined || carsInventory.length === 0) {
        return [];
    }

    const carModelNameArr = [];
    for (let i = 0; i < carsInventory.length; i++) {
        const carModelName = carsInventory[i]["car_model"];
        carModelNameArr.push(carModelName);

    }
    
    carModelNameArr.sort(function (a, b) {
        return a.toLowerCase().localeCompare(b.toLowerCase());
    });

    return carModelNameArr;    

}

// carListingAlphabetically(carsInventory);

module.exports = carListingAlphabetically;