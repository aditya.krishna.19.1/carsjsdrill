// return example -> "Last car is a *car make goes here* *car model goes here*"

// const carsInventory = require("./cars.js");

function lastCarInTheInventory(carsInventory) {
    
    if (carsInventory === undefined || carsInventory.length === 0) {
        return [];
    }

    const carEntry = carsInventory[carsInventory.length-1];
    // console.log(carEntry);
    // const carMake = carEntry["car_make"];
    // const carModel = carEntry["car_model"];
    return carEntry;
}

// console.log(lastCarInTheInventory(carsInventory));

module.exports = lastCarInTheInventory;