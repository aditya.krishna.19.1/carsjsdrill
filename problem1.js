// example car entry: 
// {"id":50,"car_make":"Lincoln","car_model":"Town Car","car_year":1999}
// return example -> console.log(`Car ${carId} is a ${carYear} ${carMake} ${carModel}`)

// const carsInventory = require("./cars.js");

function carDetailsById (carsInventory, idNumber){
    
    if (carsInventory === undefined ) {//|| carsInventory.length === 0 || idNumber === undefined || idNumber > carsInventory.length) {
        return [];
    }
    if (carsInventory.length === 0 || idNumber === undefined) {
        return [];
    }
    
    for (let i = 0; i < carsInventory.length; i++) {
        const carEntry = carsInventory[i];
        if (carEntry.id === idNumber){
            return carEntry;
        }
    }

    // console.log(idNumber);
    // const result = carsInventory[idNumber-1];
    // return result;

    // if(idNumber >= 0 && idNumber <= 49){
    //     idNumber -= 1; //matching the idNumber with correct array index
    //     const carEntry = carsInventory[idNumber]; 
    //     // console.log(carEntry);
    //     const carId = carEntry["id"];
    //     const carMake = carEntry["car_make"];
    //     const carModel = carEntry["car_model"];
    //     const carYear = carEntry["car_year"];
    //     return `Car ${carId} is a ${carYear} ${carMake} ${carModel}`
    // }
    // else{
    //     return `There is no car with id number ${idNumber}`;
    // }
}

// console.log(carDetailsById(carsInventory, 33));

module.exports = carDetailsById;