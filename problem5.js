// const carsInventory = require("./cars.js");
const carYears = require("./problem4.js")


function carsMadeBeforeYear2000 (carsInventory) {
    
    if(carsInventory === undefined || carsInventory.length === 0) {
        return [];
    }

    const allCarYears = carYears(carsInventory);
    // console.log(allCarYears);
    
    const carsMadeBeforeYear2000Arr = [];
    
    const carYearsBefore2000 = [];
    for (let i = 0; i < allCarYears.length; i++) {
        if(allCarYears[i] < 2000) {
            carYearsBefore2000.push(allCarYears[i]);
        }
    }
    
    for(let j = 0; j < carYearsBefore2000.length;){
        for (let i = 0; i < carsInventory.length;) {
            // console.log(carsInventory[i]["car_year"], carYearsBefore2000[j])
            if(carsInventory[i]["car_year"] !== carYearsBefore2000[j]){
                carsInventory.shift();

            }else{
                carsMadeBeforeYear2000Arr.push(carsInventory[i]);
                carsInventory.shift();
                carYearsBefore2000.shift();
                
            }
        }    
    }

    return carsMadeBeforeYear2000Arr;

}

// console.log(carsMadeBeforeYear2000(carsInventory).length);

module.exports = carsMadeBeforeYear2000;