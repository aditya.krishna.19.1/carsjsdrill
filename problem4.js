// const carsInventory = require("./cars.js");

function carYears(carsInventory) {
    
    if(carsInventory === undefined || carsInventory.length === 0){
        return [];
    }

    const carYearsArr = [];
    for (let i = 0; i < carsInventory.length; i++) {
        const carYear = carsInventory[i]["car_year"];
        carYearsArr.push(carYear);
    }

    return carYearsArr;
}

// console.log(carYears(carsInventory));

module.exports = carYears;